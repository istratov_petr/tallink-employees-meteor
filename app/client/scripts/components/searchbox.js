import { Template } from 'meteor/templating';

function Search(string) {
    res = [];
    Store.state.get('_.employees').forEach((item) => {
        if (item.name.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
            res.push(item);
        }
    });
    return res;
}


Template.searchbox.helpers({
    helpers: () => {
        if (Store.state.get('searchbox.focused'))
            return 'show';
    },
    matches: () => { return Store.state.get('searchbox.founded').length },
    next: () => {
        const founded = Store.state.get('searchbox.founded');
        const current = Store.state.get('searchbox.current');
        if (founded.length === 0 || founded.length === (current+1))
            return 'disabled'
    },
    prev: () => {
        const founded = Store.state.get('searchbox.founded');
        const current = Store.state.get('searchbox.current');
        if (founded.length === 0 || !current)
            return 'disabled'
    }

});

Template.searchbox.events({
    'focus .search'(event) {
        Store.state.set('searchbox.focused', true);
    },
    'blur .search'(event) {
        if (!event.target.value)
            Store.state.set('searchbox.focused', false);
    },
    'input .search'(event) {
        if (event.target.value) {
            let founded = Search(event.target.value);
            Store.state.set('searchbox.founded', founded);
            if (founded.length)
                Store.state.set('searchbox.current', 0);
            else
                Store.state.set('searchbox.current', null);
        } else {
            Store.state.set('searchbox.founded', []);
            Store.state.set('searchbox.current', null);
        }
    },
    'click .next'(event) {
        let current = Store.state.get('searchbox.current');
        if (current === null)
            current = 0;
        else
            current++;
        Store.state.set('searchbox.current', current)
    },
    'click .prev'(event) {
        let current = Store.state.get('searchbox.current');
        current--;
        Store.state.set('searchbox.current', current)
    }
});
