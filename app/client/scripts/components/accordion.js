import { Template } from 'meteor/templating';

const buttons = [
    {
        id: 'addEmployee',
        text: 'Add Employee',
        class: 'green',
        visibility: 'show',
    },
    {
        id: 'addConnection',
        text: () => {
            if (!Store.state.get('network.addEdgeMode'))
                return 'Add Connection';
            else
                return 'Cancel adding mode';
        },
        class: () => {
            if (!Store.state.get('network.addEdgeMode'))
                return 'green';
            else
                return 'red';
        },
        visibility: 'show'
    },
    {
        id: 'editEmployee',
        text: 'Edit Employee',
        class: 'yellow',
        visibility: () => {
            if (Store.state.get('network.employeesSelected').length === 1)
                return 'show'
        },
    },
    {
        id: 'delete',
        text: 'Delete selected',
        class: 'red',
        visibility: () => {
            if (Store.state.get('network.totalSelected') > 0)
                return 'show'
        },
    },
    {
        id: 'clearConnections',
        text: () => {
            return 'Clear Connections ('+ Store.state.get('network.connectionsSelected').length +')'
        },
        class: 'red',
        visibility: () => {
            if (Store.state.get('network.connectionsSelected').length > 0)
                return 'show'
        }
    }
];

Template.accordion.helpers({
    buttons() {
        let arr = [];
        const form = Store.state.get('popup.form');
        buttons.forEach((button,key) => {
            arr[key] = button;
            if (form === button.id) {
                arr[key].form = true;
                arr[key].formVisibility = Store.state.get('popup.visibility');
            }
        });
        return arr;
    }
});

Template.accordion.events({
    'click #addEmployee'(event) {
        if (!Store.state.get('popup.form')) {
            Dispatcher.dispatch({type: 'SHOW_POPUP', data: event.target.id});
            if (!Store.state.get('network.employeesSelected').length) {
                Dispatcher.dispatch({
                    type: 'SHOW_NOTIFICATION',
                    kind: 'info',
                    text: 'Select employee(s) as parent for a new one'
                });
            }
        } else {
            Dispatcher.dispatch('HIDE_POPUP');
            Dispatcher.dispatch('HIDE_NOTIFICATIONS');
        }

    },
    'click #addConnection'(event) {
        if (!Store.state.get('network.addEdgeMode')) {
            Dispatcher.dispatch({type:"DESELECT_ALL", data:'network'});
            Dispatcher.dispatch({
                type: 'SHOW_NOTIFICATION',
                kind:'info',
                text:'Click on an Employee and drag on the another Employee to connect them'
            });
            Dispatcher.dispatch('ACTIVATE_ADD_CONNECTION_MODE');
        } else {
            Dispatcher.dispatch('DEACTIVATE_ADD_CONNECTION_MODE');
            Dispatcher.dispatch('HIDE_NOTIFICATIONS');
        }
    },
    'click #editEmployee'(event) {
        if (!Store.state.get('popup.form')) {
            Dispatcher.dispatch({type: 'SHOW_POPUP', data: event.target.id});
        } else {
            Dispatcher.dispatch('HIDE_POPUP');
        }
    },
    'click #delete'(event) {
        if (!confirm('Are you sure you want to delete (' + Store.state.get('network.totalSelected') + ') selected objects?'))
            return;
        const data = {
            employees: Store.state.get('network.employeesSelected'),
            connections: Store.state.get('network.connectionsSelected'),
        };
        Dispatcher.dispatch({type: 'DELETE', data: data});
    },
    'click #clearConnections'(event) {
        if (!confirm('Are you sure you want to delete (' + Store.state.get('network.connectionsSelected').length + ') connections?'))
            return;
        const data = {
            connections: Store.state.get('network.connectionsSelected'),
        };
        Dispatcher.dispatch({type: 'DELETE', data: data});
    }
});
