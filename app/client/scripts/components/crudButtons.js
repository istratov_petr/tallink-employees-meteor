import { Template } from 'meteor/templating';

Template.crudButtons.helpers({
    editHide() {
        if (Store.state.get('typeSelector.connectionTypesSelected').length !== 1) {
            Dispatcher.dispatch('HIDE_POPUP');
            return 'disabled';
        }
    },
    deleteHide() {
        if (!Store.state.get('typeSelector.connectionTypesSelected').length)
            return 'disabled';
    }
});

Template.crudButtons.events({
    'click #addConnectionType'(event) {
        if (!Store.state.get('popup.form')) {
            Dispatcher.dispatch({type: 'SHOW_POPUP', data: event.target.id});
        } else {
            Dispatcher.dispatch('HIDE_POPUP');
        }
    },
    'click #editConnectionType'(event) {
        if (!Store.state.get('popup.form')) {
            Dispatcher.dispatch({type: 'SHOW_POPUP', data: event.target.id});
        } else {
            Dispatcher.dispatch('HIDE_POPUP');
        }
    },
    'click #deleteConnectionType'(event) {
        const selected = Store.state.get('typeSelector.connectionTypesSelected');
        if (selected.find('0')) {
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'warn', text:'You cannot delete default connection type'});
            return;
        }
        if (!confirm('Are you sure you want to delete this connection type, all related connections of this type probably would be deleted, and there is no turning back!'))
            return;
        Dispatcher.dispatch({
            type: 'DELETE_CONNECTION_TYPE',
            data: selected
        });
    }
});