import { Template } from 'meteor/templating';

function getSelectionComponent(entity) {
    let res = '';
    _.map( Store.model, (value, component) => {
        if (component === '_') return;
        _.map(value, (item, key) => {
            if (key === entity + 'Selected') res = component;
        });
    });
    return res;
}

function getFields(entity) {
    let res = [];
    if (!Store.model._[entity]) return;
    Store.model._[entity].fields.forEach((field) => {
        if (field.formType === "checkbox") {
            field.checked = () => {
                if ( getValue(field, entity) )
                    return 'checked';
                else
                    return '';
            };

        } else {
            field.value = () => {
                return getValue(field, entity);
            };
        }

        res.push(field);
    });
    return res;
}

function getValue(field, entity) {
    const component = getSelectionComponent(entity);
    const keySelected = entity + 'Selected';
    const selectedIds = Store.state.get(component + '.' + keySelected);
    if (Store.state.get('popup.form').match(/edit*/) &&  selectedIds.length === 1) {
        return Store.getById(selectedIds[0], entity)[field.name];
    } else {
        return '';
    }
}

function getOwner(entity) {
    const component = getSelectionComponent(entity);
    const keySelected = entity + 'Selected';
    const selectedIds = Store.state.get(component + '.' + keySelected);
    if (Store.state.get('popup.form').match(/edit*/) &&  selectedIds.length === 1)
        return Store.getById(selectedIds[0], entity).owner;
}

const forms = {
    addEmployee: {entity: 'employees', method: 'add', dispatch: 'ADD_EMPLOYEE'},
    editEmployee: {entity: 'employees', method: 'edit', dispatch: 'UPDATE_EMPLOYEE'},
    addConnectionType: {entity: 'connectionTypes', method: 'add', dispatch: 'ADD_CONNECTION_TYPE'},
    editConnectionType: {entity: 'connectionTypes', method: 'edit', dispatch: 'UPDATE_CONNECTION_TYPE'},
};

let fieldsTypes = {
    text: { value: 'value', default: '' },
    hidden: { value: 'value', default: '' },
    checkbox: { value: 'checked', default: false }
};


Template.popup.helpers({
    id() { return Store.state.get('popup.form') },
    fields () {
        const form = Store.state.get('popup.form');
        if (form) {
            const fields = getFields(forms[form].entity);
            let res = [];
            fields.forEach((field) => {
                let newField = _.clone(field);
                if ( forms[form].method !== 'add' &&
                    newField.onlyForOwner &&
                    getOwner(forms[form].entity) !== Meteor.userId() ) {
                    if (newField.formType === 'checkbox') newField.value = getValue(field, forms[form].entity);

                    newField.formType = 'hidden';
                }
                if (newField.required) newField.required = "required";
                res.push(newField);
            });
            return res;
        }
    },
    equals(v1, v2) {
        return (v1 === v2);
    }
});

Template.popup.events({
    'submit' (event) {
        event.preventDefault();
        let newData = {};
        let oldData = {};
        const form = event.target.className;
        getFields(forms[form].entity).forEach((field) => {
            let fieldType = event.target[field.name].type;
            newData[field.name] = event.target[field.name][fieldsTypes[fieldType].value];
            if (forms[form].method === 'add') {
                event.target[field.name][fieldsTypes[fieldType].value] = fieldsTypes[fieldType].default;
            }
            if (forms[form].method === 'edit') oldData[field.name] = field[fieldsTypes[fieldType].value];
        });
        if (forms[form].method === 'add' || !_.isEqual(newData,oldData)) {
            if (newData.privateSwitch === "" ) newData.privateSwitch = false;
            validate.async(newData, Validator.constraints[forms[form].entity]).then(() => {
                const sanitizedData = Validator.sanitize(newData);
                Validator.validate(sanitizedData, forms[form].entity, () => {
                    Dispatcher.dispatch({type: forms[form].dispatch, data: sanitizedData});
                    Dispatcher.dispatch('HIDE_POPUP');
                });
            }, (error) => {
                _.map(error, (message) => {
                    Dispatcher.dispatch({type: 'SHOW_NOTIFICATION', kind: 'warn', text: message[0]})
                });
            });
        }
    }
});
