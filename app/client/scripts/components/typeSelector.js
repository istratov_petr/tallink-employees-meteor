import { Template } from 'meteor/templating';

Template.typeSelector.helpers({
    styling_id: 'updateConnectionTypes',
    class() {
        const form = Store.state.get('popup.form');
        if (form === 'addConnectionType') return 'green';
        if (form === 'editConnectionType') return 'yellow';
    },
    formVisibility() {
        const form = Store.state.get('popup.form');
        if (form.endsWith('ConnectionType'))
            return Store.state.get('popup.visibility');
    },
    connectionTypes() {
        let types = Store.state.get('_.connectionTypes');
        types.forEach((type) => {
            type.selected = () => {
                let res = '';
                Store.state.get('typeSelector.connectionTypesSelected').forEach((item) => {
                    if (item === type._id) res = 'selected';
                });
                return res;
            };
        });
        return types;
    }
});

Template.typeSelector.events({
    'click .typeSelector'(event) {
        if (event.target.id === '') return;
        if (Store.state.get('network.addEdgeMode')) return;
        const shiftPressed = Store.state.get('keys.shiftPressed');
        const selected = Store.state.get('typeSelector.connectionTypesSelected');
        if ( !shiftPressed && Store.state.get('typeSelector.connectionTypesSelected').length)
            Dispatcher.dispatch({type:"DESELECT_ALL", data:'all'});

        if (![event.target.id].intersect(selected).length)  {
            Dispatcher.dispatch({
                type: "SELECT",
                data: {
                    key: 'typeSelector.connectionTypesSelected',
                    array: [event.target.id]
                }
            });
        } else {
            Dispatcher.dispatch({
                type: "DESELECT",
                data: {
                    key: 'typeSelector.connectionTypesSelected',
                    array: [event.target.id]
                }
            });
        }
    }
});