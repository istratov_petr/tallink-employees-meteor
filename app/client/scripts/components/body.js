import { Template } from 'meteor/templating';

Template.body.onRendered(() => {
    Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'info', text:'Loading...'});
    Dispatcher.dispatch({ type: "LOAD_DATA", data: "all" });
});
