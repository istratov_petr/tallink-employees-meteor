import { Template } from 'meteor/templating';
import { Accounts } from 'meteor/accounts-base';

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY',
});

Template.loginButtonsOverriden.replaces('loginButtons');
Template._loginButtonsLoggedOutDropdownOverriden.replaces('_loginButtonsLoggedOutDropdown');
Template._loginButtonsFormFieldOverriden.replaces('_loginButtonsFormField');
Template._loginButtonsLoggedInDropdownOverriden.replaces('_loginButtonsLoggedInDropdown');

Accounts.onLogin(() => {
    Dispatcher.dispatch('UPDATE_NETWORK');
});

Accounts.onLogout(() => {
    Dispatcher.dispatch('UPDATE_NETWORK');
    Store.state.set('typeSelector.connectionTypesSelected', ["0"]);
});