import { Meteor } from 'meteor/meteor';
import LocalStore from './localStore'

Store = new LocalStore();
Validator = new _Validator;

Meteor.startup(() => {
    _.extend(Notifications.defaultOptions, {
        timeout: 5000
    });
    Notifications.defaultOptionsByType[Notifications.TYPES.INFO] = _.defaults({
            timeout: 0
        },
        Notifications.defaultOptions);
});

document.addEventListener("keydown", (event) => {
    const keys = [91,93,17,16];
    keys.forEach((key) => {
        if (event.which === key)
            Store.state.set( 'keys.shiftPressed', true );
    });
});
document.addEventListener("keyup", (event) => {
    Store.state.set( 'keys.shiftPressed', false );
});