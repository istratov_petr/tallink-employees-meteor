class LocalStore extends _AppStore {
    constructor() {
        const model = {
            _: {
                employees: {
                    data: [],
                    fields: [
                        {
                            formType: 'hidden',
                            name: '_id',
                            dataType: String
                        },
                        {
                            formType: 'text',
                            name: 'name',
                            dataType: String,
                            label: 'Full name',
                            constraint: {
                                presence: true,
                                length: {
                                    maximum: 39,
                                    message: 'must not be longer then 39 characters'
                                },
                                wordsLength: {
                                    maximum: 19,
                                    message: 'maximum word length is 19 characters'
                                }
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'text',
                            name: 'position',
                            label: 'Position',
                            dataType: String,
                            constraint: {
                                presence: true,
                                length: {
                                    maximum: 49,
                                    message: 'must not be longer then 49 characters'
                                },
                                wordsLength: {
                                    maximum: 24,
                                    message: 'maximum word length is 24 characters'
                                }
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'checkbox',
                            name: 'privateSwitch',
                            label: 'Private',
                            dataType: Boolean,
                            onlyForOwner: true,
                            constraint: {
                                presence: false
                            },
                            validate: {
                                isBoolean: {
                                    message: 'Value should be a boolean'
                                }
                            }
                        },
                        {
                            formType: 'hidden',
                            name: 'owner',
                            dataType: String
                        },
                        {
                            formType: 'hidden',
                            name: 'userName',
                            dataType: String
                        }
                    ],
                    filters: {}
                },
                connections: {
                    data: [],
                    fields: [
                        {
                            formType: 'hidden',
                            name: '_id',
                            dataType: String
                        },
                        {
                            formType: 'text',
                            name: 'from',
                            label: 'From',
                            dataType: String,
                            constraint: {
                                presence: true
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'text',
                            name: 'to',
                            label: 'To',
                            dataType: String,
                            constraint: {
                                presence: true
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'hidden',
                            name: 'connection_types_id',
                            label: 'Type ID',
                            dataType: String,
                            constraint: {
                                presence: true
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'checkbox',
                            name: 'privateSwitch',
                            label: 'Private',
                            dataType: Boolean,
                            onlyForOwner: true,
                            constraint: {
                                presence: false
                            },
                            validate: {
                                isBoolean: {
                                    message: 'Value should be a boolean'
                                }
                            }
                        },
                        {
                            formType: 'hidden',
                            name: 'owner',
                            dataType: String
                        },
                        {
                            formType: 'hidden',
                            name: 'userName',
                            dataType: String
                        }
                    ],
                    filters: {
                        connection_types_id: '0'
                    }
                },
                connectionTypes: {
                    data: [],
                    fields: [
                        {
                            formType: 'hidden',
                            name: '_id',
                            dataType: String
                        },
                        {
                            formType: 'text',
                            name: 'name',
                            label: 'Connection type name',
                            dataType: String,
                            constraint: {
                                presence: true,
                                length: {
                                    maximum: 50,
                                    message: 'must not be longer then 50 characters'
                                },
                                wordsLength: {
                                    maximum: 25,
                                    message: 'maximum word length is 25 characters'
                                }
                            },
                            validate: {
                                isString: {
                                    message: 'Value should be a string'
                                }
                            }
                        },
                        {
                            formType: 'checkbox',
                            name: 'privateSwitch',
                            label: 'Private',
                            dataType: Boolean,
                            onlyForOwner: true,
                            constraint: {
                                presence: false
                            },
                            validate: {
                                isBoolean: {
                                    message: 'Value should be a boolean'
                                }
                            }
                        },
                        {
                            formType: 'hidden',
                            name: 'owner',
                            dataType: String
                        },
                        {
                            formType: 'hidden',
                            name: 'userName',
                            dataType: String
                        }
                    ],
                    filters: {}
                },
            },
            network: {
                employeesSelected: {data:[]},
                connectionsSelected: {data:[]},
                totalSelected: {data:0},
                addEdgeMode: {data:false}
            },
            popup: {
                form: {data:''},
                visibility: {data:''}
            },
            notification: {
                instances: {data:[]}
            },
            searchbox: {
                focused: {data:false},
                founded: {data:[]},
                current: {data:null}
            },
            typeSelector: {
                connectionTypesSelected: {data:["0"]}
            },
            keys: {
                shiftPressed: {data:false}
            }
        };
        super(model);
    }
}

export default LocalStore