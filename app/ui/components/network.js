import vis from 'vis'

class Network {
    constructor() {
        this.network = null;
        this.nodes = new vis.DataSet();
        this.edges = new vis.DataSet();
        this.rectStyle = {};
        this.nodeStyles = '';
        this.animationOptions = {
            scale: 1.0,
            offset: {x: 0, y: 0},
            animation: {
                duration: 100,
                easingFunction: 'easeInOutQuad'
            }
        };
        this.firstTimeLoaded = true;
        this.terms = {
            employees: { dataSetkey: 'nodes', methodKey: 'Node'},
            connections: { dataSetkey: 'edges', methodKey: 'Edge'}
        };
    }
    destroy() {
        if (this.network !== null) {
            this.network.destroy();
            this.network = null;
            this.nodes = [];
            this.edges = [];
        }
    }
    fetchStyles() {
        _.map(document.styleSheets[0].cssRules, (rule) => {
            if (rule.selectorText !== undefined) {
                if (rule.selectorText.match(/.visjs-*/)) {
                    this.nodeStyles += rule.cssText;
                    if (rule.selectorText === ".visjs-nodes-rect")
                        this.rectStyle.default = rule.style;
                    if (rule.selectorText === ".visjs-nodes-rect-selected")
                        this.rectStyle.selected = rule.style;
                }
            }
        });
    }
    updateDataSet(entity) {
        this[this.terms[entity].dataSetkey].clear();
        Store.state.get('_.' + entity).forEach((item) => {
            this[this.terms[entity].dataSetkey].add(this['construct' + this.terms[entity].methodKey](item))
        });
        //if (this.network) this.network.stabilize();
    }
    draw(domElement) {
        this.destroy();
        this.fetchStyles();

        _.map(this.terms, (term, entity) => { this.updateDataSet(entity) });

        const container = document.getElementById(domElement);

        let data = {
            nodes: this.nodes,
            edges: this.edges
        };

        const options = {
            layout: {
                hierarchical: {
                    sortMethod: "directed",
                    enabled: true,
                }
            },
            physics: {
                hierarchicalRepulsion: {
                    nodeDistance: 160,
                    centralGravity: 0.2,
                    springConstant: 0.02
                }
            },
            edges: {
                smooth: {
                    type: 'horizontal',
                    forceDirection: 'horizontal',
                    roundness: 1
                }
            }
        };

        this.network = new vis.Network(container, data, options);

        //this.network.stabilize();
        this.bindEvents();

    }
    addNode(data) {
        this.nodes.add(this.constructNode(data));
    }
    updateNode(data) {
        this.nodes.update(this.constructNode(data));
    }
    addEdge(data) {
        this.edges.add(this.constructEdge(data));
    }
    deleteNodes(data) {
        this.nodes.remove(data);
    }
    deleteEdges(data) {
        this.edges.remove(data);
    }
    constructNode(item) {
        const DOMURL = window.URL || window.webkitURL || window;

        const data = Blaze.toHTMLWithData(Template.node, {
            name:          item.name,
            position:      item.position,
            styles:        this.nodeStyles,
            privateSwitch: item.privateSwitch
        });

        const svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        const url = DOMURL.createObjectURL(svg);

        return {
            id:     item._id,
            shape:  'image',
            image:  url,
            shapeProperties: {
                useBorderWithImage: true,
            },
            borderWidth: parseInt(this.rectStyle.default['stroke-width']),
            borderWidthSelected: parseInt(this.rectStyle.selected['stroke-width']),
            color: {
                border: this.rectStyle.default.stroke,
                background: this.rectStyle.default.fill,
                highlight: {
                    border: this.rectStyle.selected.stroke,
                    background: this.rectStyle.selected.fill
                }
            },
            x: 0
        };
    }
    constructEdge(item) {
       return  {
           id:     item._id,
           from:   item.from,
           to:     item.to,
           arrows: {to : true }
        }
    }
    bindEvents() {
        Store.state.watchGroup([
            'network.employeesSelected',
            'network.connectionsSelected'
        ], (values) => {
            let newTotal = 0;
            _.map(values, (value) => {
                newTotal += value.length;
            });
            const oldTotal = Store.state.get('network.totalSelected');
            if (oldTotal === 0 && newTotal > 0) {
                Dispatcher.dispatch('HIDE_NOTIFICATIONS');
                Dispatcher.dispatch({
                    type: 'SHOW_NOTIFICATION',
                    kind:'info',
                    text:'Press and hold SHIFT or CTRL to select multiple objects'
                });
            }
            if (oldTotal > 0 && newTotal === 0) {
                Dispatcher.dispatch('HIDE_NOTIFICATIONS');
                Dispatcher.dispatch('HIDE_POPUP');
            }
            Store.state.set('network.totalSelected', newTotal);
            this.network.setSelection({
                nodes: values['network.employeesSelected'],
                edges: values['network.connectionsSelected']
            });
        });

        this.network.on("click", (params) => {
            if (Store.state.get('network.addEdgeMode')) return;
            const keys = {
                employees: 'nodes',
                connections: 'edges'
            };

            const shiftPressed = Store.state.get('keys.shiftPressed');

            let selected = {};

            if ( !shiftPressed && Store.state.get('network.totalSelected'))
                Dispatcher.dispatch({type:"DESELECT_ALL", data:'network'});

            _.map(keys, (visKey, key) => {
                const keySelected = 'network.' + key + 'Selected';
                selected[keySelected] = Store.state.get(keySelected);
                if (params[visKey].length) {
                    if ((!params[visKey].intersect(selected[keySelected]).length) ||
                        (visKey === "edges" && shiftPressed && params.nodes.length)) {
                        Dispatcher.dispatch({type: "SELECT", data: {key: keySelected, array: params[visKey]}});
                    } else {
                        Dispatcher.dispatch({type: "DESELECT", data: {key: keySelected, array: params[visKey]}});
                    }
                }
            });

        });

        this.network.on("doubleClick", (params) => {
            if (params.nodes.length) {
                this.network.focus(params.nodes[0], this.animationOptions);
            }
        });

        Store.state.watch('searchbox.current',(key) => {
            if (key !== null) {
                const currentEmployee = Store.state.get('searchbox.founded')[key];
                this.network.focus(currentEmployee._id, this.animationOptions);
            }
        });

        Store.state.watch('typeSelector.connectionTypesSelected',(key) => {
            if (key.length !== 1) return;
            Store.state.set('_.connections.filters', { connection_types_id: key[0]});
            Store.getData('connections' , () => {
                this.updateDataSet('connections');
                Dispatcher.dispatch('HIDE_NOTIFICATIONS');
            });
        });

    }
    addEdgeMode(toogle) {
        const manipulation = this.network.manipulation;

        manipulation.options.addEdge = (data, cb) => {
            if (data.from === data.to) return;
            Dispatcher.dispatch({type: "ADD_CONNECTION", data: data});
            Dispatcher.dispatch('DEACTIVATE_ADD_CONNECTION_MODE');
            Dispatcher.dispatch('HIDE_NOTIFICATIONS');
        };

        if (toogle) {
            manipulation.inMode = 'addEdge';
            manipulation._temporaryBindUI('onDragStart', manipulation._handleConnect.bind(manipulation));
            manipulation._temporaryBindUI('onDragEnd', manipulation._finishConnect.bind(manipulation));
            manipulation._temporaryBindUI('onDrag', manipulation._dragControlNode.bind(manipulation));
            manipulation._temporaryBindUI('onRelease', manipulation._finishConnect.bind(manipulation));

            manipulation._temporaryBindUI('onTouch', () => {});
            manipulation._temporaryBindUI('onHold', () => {});
        } else {
            manipulation.inMode = false;
            manipulation._unbindTemporaryUIs();
        }
    }

}

export default Network;