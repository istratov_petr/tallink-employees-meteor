import { check } from 'meteor/check'

class AppValidator {
    constructor() {
        this.constraints = this.buildConstraints();
        this.validators = this.buildValidators();
        this.dataTypes = this.buildDataTypes();
        this.setCustomValidators();
        validate.isBoolean = (value) => {
            return _.isBoolean(value);
        };
    }
    buildConstraints() {
        let res = {};
        _.map( Store.model._, (data, entity) => {
            res[entity] = {};
            data.fields.forEach((field) => {
                res[entity][field.name] = field.constraint;
            })
        });
        return res;
    }
    setCustomValidators() {
        validate.validators.wordsLength = (value,options) => {
            return new validate.Promise((resolve, reject) => {
                value.words().forEach((word) => {
                    if (word.length > options.maximum)
                        resolve(options.message);
                });
                resolve();
            });
        };
    }
    buildValidators() {
        let res = {};
        _.map( Store.model._, (data, entity) => {
            res[entity] = {};
            data.fields.forEach((field) => {
                res[entity][field.name] = field.validate;
            })
        });
        return res;
    }
    validate(form, entity, cb) {
        let res = true;
        _.map(form, (value, field) => {
            _.map(this.validators[entity][field], (data, validator) => {
                if (!validate[validator](value)) {
                    res = false;
                    Dispatcher.dispatch({type: 'SHOW_NOTIFICATION', kind: 'warn', text: data.message})
                }
            });
        });
        if (res) cb();
    }
    sanitize(data) {
        let res = {};
        _.map(data, (value, field) => {
            if (validate.isString(value))
                res[field] = value.compact().stripTags();
            else
                res[field] = value;
        });

        return res;
    }
    buildDataTypes() {
        let res = {};
        _.map( Store.model._, (data, entity) => {
            res[entity] = {};
            data.fields.forEach((field) => {
                res[entity][field.name] = field.dataType;
            })
        });
        return res;
    }
    checkDataTypes(data, entity) {
        data.forEach((row) => {
            _.map(row.$set ? row.$set : row, (value,key) => {
                let dataType;
                if (key === 'createdAt')
                    dataType = Date;
                else
                    dataType = this.dataTypes[entity][key];
                check(value, dataType);
            });
        });
    }
}
_Validator = AppValidator;