import Network from '../ui/components/network.js';

_Network = new Network;

Dispatcher.register((action) => {
    console.log( '-> ' + action.type);
    let selected = [];
    const newConnections = [];
    let newData = {};
    const connectionTypesSelected = Store.state.get('typeSelector.connectionTypesSelected');
    switch( action.type ) {
        case "LOAD_DATA":
            Store.getData(action.data, () => {
                Dispatcher.dispatch( 'HIDE_NOTIFICATIONS' );
                Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text:'Data has been succesfully Loaded'});
                Dispatcher.dispatch( 'SHOW_NETWORK' );
            });
            break;
        case "SHOW_NETWORK":
            _Network.draw('network');
            break;
        case "UPDATE_NETWORK":
            Store.getData('all', () => {
                _Network.updateDataSet('employees');
                _Network.updateDataSet('connections');
            });
            break;
        case "SELECT":
            selected = Store.state.get(action.data.key).union(action.data.array);
            Store.state.set(action.data.key, selected);
            break;
        case "DESELECT":
            selected = _.difference(Store.state.get(action.data.key), action.data.array);
            Store.state.set(action.data.key, selected);
            break;
        case "DESELECT_ALL":
            const selectedEntities = [
                'network.employeesSelected',
                'network.connectionsSelected',
                'typeSelector.connectionTypesSelected'
            ];

            selectedEntities.forEach((key) => {
                if (action.data === 'all' || key.startsWith(action.data))
                    Store.state.set(key, []);
            });

            break;
        case "SHOW_NOTIFICATION":
            let _notifications = [];
            _notifications = Store.state.get('notification.instances')
                .union(Notifications[action.kind](action.text));
            Store.state.set('notification.instances', _notifications);
            break;
        case "HIDE_NOTIFICATIONS":
            Store.state.get('notification.instances').forEach((id) => {
                Notifications.remove({ _id: id });
            });
            Store.state.set('notification.instances', []);
            break;
        case "SHOW_POPUP":
            Store.state.set('popup.form', action.data);
            setTimeout(() => {
                Store.state.set('popup.visibility', "show");
            }, 2);
            break;
        case "HIDE_POPUP":
            Store.state.set('popup.visibility', '');
            setTimeout(() => {
                Store.state.set('popup.form', '');
            }, 300);
            break;
        case "ADD_EMPLOYEE":
            const newEmployee = {
                _id:            (Math.random() * 1e7).toString(32),
                name:           action.data.name,
                position:       action.data.position,
                createdAt:      new Date(),
                owner:          Meteor.userId(),
                userName:       Meteor.users.findOne(Meteor.userId).username,
                privateSwitch:  action.data.privateSwitch
            };

            _Network.addNode(newEmployee);
            Store.updateEmployees([newEmployee]);

            const employeesSelected = Store.state.get('network.employeesSelected');

            if (employeesSelected.length) {
                employeesSelected.forEach((employee) => {
                    connectionTypesSelected.forEach((connectionType) => {
                        const edge = {
                            _id:                    (Math.random() * 1e7).toString(32),
                            connection_types_id:    connectionType,
                            from:                   employee,
                            to:                     newEmployee._id,
                            createdAt:              new Date(),
                            owner:                  Meteor.userId(),
                            userName:               Meteor.users.findOne(Meteor.userId).username,
                            privateSwitch:          newEmployee.privateSwitch
                        };
                        _Network.addEdge(edge);
                        newConnections.push(edge);
                    });
                });
                Store.addConnections(newConnections);
            }
            break;
        case "ADD_CONNECTION":
            connectionTypesSelected.forEach((connectionType) => {
                const newConnection = {
                    _id:                    (Math.random() * 1e7).toString(32),
                    connection_types_id:    connectionType,
                    from:                   action.data.from,
                    to:                     action.data.to,
                    createdAt:              new Date(),
                    owner:                  Meteor.userId(),
                    userName:               Meteor.users.findOne(Meteor.userId).username,
                    privateSwitch:          Store.getById(action.data.from,'employees').privateSwitch || false
                };

                newConnections.push(newConnection);
            });
            _Network.addEdge(action.data);
            Store.addConnections(newConnections);
            break;
        case "ADD_CONNECTION_TYPE":
            Store.updateConnectionTypes([{
                _id:            (Math.random() * 1e7).toString(32),
                name:           action.data.name,
                createdAt:      new Date(),
                owner:          Meteor.userId(),
                userName:       Meteor.users.findOne(Meteor.userId).username,
                privateSwitch:  action.data.privateSwitch
            }]);
            break;
        case "UPDATE_EMPLOYEE":
            _Network.updateNode(action.data);
            newData = {
                _id: action.data._id,
                name:           action.data.name,
                position:       action.data.position,
                privateSwitch:  action.data.privateSwitch
            };
            Store.updateEmployees([newData]);
            break;
        case "UPDATE_CONNECTION_TYPE":
            newData = {
                _id: action.data._id,
                name:          action.data.name,
                privateSwitch: action.data.privateSwitch
            };
            Store.updateConnectionTypes([newData]);
            break;
        case "DELETE_CONNECTION_TYPE":
            Store.deleteConnectionTypes(action.data);
            Store.state.set('typeSelector.connectionTypesSelected', ["0"]);
            break;
        case "DELETE":
            new Promise((resolve, reject) => {
                _Network.deleteNodes(action.data.employees);
                _Network.deleteEdges(action.data.connections);
                resolve();
            }).then(() => {
                Dispatcher.dispatch({type:"DESELECT_ALL", data:'network'});
            });
            Store.delete(action.data);
            break;
        case "ACTIVATE_ADD_CONNECTION_MODE":
            Store.state.set('network.addEdgeMode', true);
            _Network.addEdgeMode(true);
            break;
        case "DEACTIVATE_ADD_CONNECTION_MODE":
            Store.state.set('network.addEdgeMode', false);
            _Network.addEdgeMode(false);
            break;
    }
});