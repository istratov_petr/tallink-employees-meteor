import { ReactiveDict } from 'meteor/reactive-dict';

class AppStore {
    constructor(mode) {
        this.model = mode;
        this.state = new ReactiveDict();
        _.map( this.model, (group, keyGroup) => {
            _.map(group, (value, key) => {
                this.state.set(keyGroup + '.' + key, value.data);
                if (value.filters) this.state.set(keyGroup + '.' + key + '.filters', value.filters);
            });
        });
    }
    getData(action, cb) {
        let actions;
        let promises = [];
        if (action === 'all') {
            actions = Object.keys(this.model._);
        } else {
            actions = [action];
        }
        actions.forEach((action) => {
            const filters = this.state.get('_.' + action + '.filters');
            promises.push(
                new Promise((resolve, reject) => {
                    Meteor.call(action + '.list', {filters: filters}, (err, result) => {
                        if (err !== undefined) {
                            reject(err);
                        }
                        resolve(result);
                    })
                }).then((result) => {
                    this.state.set('_.' + action, result);
                }, (err) => {
                    Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
                })
            );
        });
        Promise.all(promises).then(() => {
            if (cb !== undefined)
                return cb();
        });
    }
    updateEmployees(data) {
        Validator.checkDataTypes(data, 'employees');

        new Promise((resolve, reject) => {
            Meteor.call('employees.update', data, (err, result) => {
                if (err !== undefined) {
                    reject(err);
                }
                resolve(result);
            })
        }).then((result) => {
            this.getData('employees');
            Dispatcher.dispatch( 'HIDE_NOTIFICATIONS' );
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text: 'Employees has been updated successfully!' });
        }, (err) => {
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
        });
    }
    updateConnectionTypes(data) {
        Validator.checkDataTypes(data, 'connectionTypes');
        new Promise((resolve, reject) => {
            Meteor.call('connectionTypes.update', data, (err, result) => {
                if (err !== undefined) {
                    reject(err);
                }
                resolve(result);
            })
        }).then((result) => {
            this.getData('connectionTypes');
            Dispatcher.dispatch( 'HIDE_NOTIFICATIONS' );
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text: 'Connection types has been updated successfully!' });
        }, (err) => {
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
        });
    }
    addConnections(data) {
        Validator.checkDataTypes(data, 'connections');
        new Promise((resolve, reject) => {
            Meteor.call('connections.add', data, (err, result) => {
                if (err !== undefined) {
                    reject(err);
                }
                resolve(result);
            })
        }).then((result) => {
            this.getData('connections');
            Dispatcher.dispatch( 'HIDE_NOTIFICATIONS' );
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text: 'New Connection(s) has been added successfully!' });
        }, (err) => {
            Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
        });
    }
    getById(id, entity) {
        let res = {};
        this.state.get('_.' + entity).forEach((item) => {
            if (item._id === id) {
                res = item;
            }
        });
        return res;
    }
    delete(data) {
        let isReplacedConnections = false;
        if (data.employees && data.employees.length) {
            new Promise((resolve, reject) => {
                Meteor.call('employees.delete', data.employees, (err, result) => {
                    if (err !== undefined) {
                        reject(err);
                    }
                    resolve(result);
                })
            }).then((result) => {
                this.getData('employees', () => {
                    Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text: 'Employee(s) had been deleted successfully!' });
                });
            }, (err) => {
                Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
            });
            isReplacedConnections = this.checkBrokenConnections(data.employees);
        }


        if (data.connections && data.connections.length) {
            new Promise((resolve, reject) => {
                Meteor.call('connections.delete', data.connections, (err, result) => {
                    if (err !== undefined) {
                        reject(err);
                    }
                    resolve(result);
                })
            }).then((result) => {
                Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'info', text:'Loading...'});
                this.getData('connections', () => {
                    Dispatcher.dispatch( 'HIDE_NOTIFICATIONS' );
                    if (isReplacedConnections) _Network.updateDataSet('connections');
                    Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'success', text: 'Connections had been deleted successfully!' });
                });
            }, (err) => {
                Dispatcher.dispatch({ type: 'SHOW_NOTIFICATION', kind:'error', text:err });
            });
        }
    }

    deleteConnectionTypes(data) {
        if (!data.length) return;
        new Promise((resolve, reject) => {
            Meteor.call('connectionTypes.delete', data, (err, result) => {
                if (err !== undefined) {
                    reject(err);
                }
                resolve(result);
            })
        }).then((result) => {
            this.getData('connectionTypes');
            Dispatcher.dispatch({
                type: 'SHOW_NOTIFICATION',
                kind: 'success',
                text: 'Connection type(s) had been deleted successfully!'
            });
        }, (err) => {
            Dispatcher.dispatch({type: 'SHOW_NOTIFICATION', kind: 'error', text: err});
        });
    }

    checkBrokenConnections(data) {
        let from = false;
        let to = false;
        data.forEach((item) => {
            if (this.find('connections', [{key: 'from', value: item}]).length) from = true;
            if (this.find('connections', [{key: 'to', value: item}]).length) to = true;
        });
        return !!(from && to);
    }
    find(entity, where) {
        let res = [];
        Store.state.get('_.' + entity).forEach((item) => {
            where.forEach((query) => {
                if (item[query.key] === query.value) res.push(item);
            })
        });
        return res;
    }
}
_AppStore = AppStore;
