import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Connections = new Mongo.Collection('connections');

ApiConnections = "http://localhost:8888/api/connections";

Meteor.methods ({
    'connections.list'(args) {
        let query = {
            $or: [
                { privateSwitch: { $ne: true } },
                { owner:         this.userId },
            ]
        };
        if (args.filters) {
            _.map(args.filters, (value,field) => {
                if (Array.isArray(value)) {
                    query[field] = { $in: value };
                } else {
                    query[field] = value;
                }
            });
        }

        return HTTP.get(ApiConnections, {data: query}).data;
    },
    'connections.add'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        HTTP.post(ApiConnections, {data: data});
    },
    'connections.delete'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        HTTP.del(ApiConnections, {data: data});
    }
});
