import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const ConnectionTypes = new Mongo.Collection('connection_types');

ApiConnectionTypes = "http://localhost:8888/api/connection_types";

Meteor.methods ({
    'connectionTypes.list'(args) {
        let query = {
            $or: [
                { privateSwitch: { $ne: true } },
                { owner:         this.userId },
            ]
        };
        if (args.filters) {
            _.map(args.filters, (value,field) => {
                if (Array.isArray(value)) {
                    query[field] = { $in: value };
                } else {
                    query[field] = value;
                }
            });
        }

        return HTTP.get(ApiConnectionTypes, {data: query}).data;
    },
    'connectionTypes.update'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        HTTP.post(ApiConnectionTypes, {data: data});
    },
    'connectionTypes.delete'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        HTTP.del(ApiConnectionTypes, {data: data});
    }
});