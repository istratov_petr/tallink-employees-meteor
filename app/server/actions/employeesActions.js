import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Employees = new Mongo.Collection('employees');

ApiEmployees = "http://localhost:8888/api/employees";
ApiConnections = "http://localhost:8888/api/connections";

Meteor.methods ({
    'employees.list'(args) {
        let query = {
            $or: [
                { privateSwitch: { $ne: true } },
                { owner:         this.userId },
            ]
        };
        if (args.filters) {
            _.map(args.filters, (value,field) => {
                if (Array.isArray(value)) {
                    query[field] = { $in: value };
                } else {
                    query[field] = value;
                }
            });
        }

        return HTTP.get(ApiEmployees, {data: query}).data;

    },
    'employees.update'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        HTTP.post(ApiEmployees, {data: data});

    },
    'employees.delete'(data) {
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        HTTP.del(ApiEmployees, {data: data});
    }
});
