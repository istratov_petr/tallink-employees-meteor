# Employees Demo Meteor App @

This Demo App shows how to design and implement Meteor Application with Flux-oriented architecture using external REST API

## Prerequisites:
* Git
* NodeJS 6
* Meteor 1.4
* Python 2.7
* Make
* G++ ^4.7

## Install and run the project 

This instruction works for clean machine with Linux OS installed

### Download and Install NodeJs

```
[~] # curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
[~] # sudo apt-get install -y nodejs
[~] # sudo apt-get install -y build-essential
```

> **Important!:** Check your version of G++ via `[~] # g++ -v` it should not be lower than 4.7

### Download and Install Meteor

```
[~] # curl https://install.meteor.com/ | sh
```

### Download and Install Python

```
[~] # cd /opt/
[opt] # wget https://www.python.org/ftp/python/2.7.12/Python-2.7.12.tgz
[opt] # tar xzf Python-2.7.12.tgz
[opt] # cd Python-2.7.12
[opt] # ./configure
[opt] # make
[opt] # make install
```

### Download and Install Git

```
[~] # apt-get install git
```

### Clone the project

```
[~] # git clone git@bitbucket.org:istratov_petr/tallink-employees-meteor.git
```

### Start Application

```
[~] # cd tallink-employees-meteor/app
[app] # meteor npm install
[app] # meteor
```

Now the Meteor Application is up and running on `localhost:3000` 
  
> **Note:** This Application will work properly only in pair with REST API example described here https://bitbucket.org/istratov_petr/tallink-employees-api

## Architecture scheme

![Architecture scheme](http://smages.com/images/tallinkemp.png)